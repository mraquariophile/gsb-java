import java.awt.Button;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Label;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class Home extends JFrame {

	private JPanel contentPane;
	private JTextField textName;
	private JPasswordField TxtPass;
	int xx,xy ;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Home frame = new Home();
					frame.setUndecorated(true);
					frame.setVisible(true);
					
					URL iconURL = getClass().getResource("/img/icon.png");
					// iconURL is null when not found
					ImageIcon icon = new ImageIcon(iconURL);
					frame.setIconImage(icon.getImage());
 
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Home() {
		setType(Type.POPUP);
		setTitle("Gestion ");
		setBackground(Color.WHITE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 788, 552);
		contentPane = new JPanel();
		contentPane.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				 xx = e . getX ();
			     xy = e . getY ();
			}
		});
		contentPane.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent arg0) {
				int x = arg0.getXOnScreen();
	            int y = arg0.getYOnScreen();
	            Home.this.setLocation(x - xx, y - xy); 
			}
		});
		contentPane.setBackground(new Color(230, 230, 250));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				 xx = e . getX ();
			     xy = e . getY ();
			}
		});
		panel.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent arg0) {
				
				int x = arg0.getXOnScreen();
	            int y = arg0.getYOnScreen();
	            Home.this.setLocation(x - xx, y - xy);  
			}

		});
		panel.setBackground(Color.BLACK);
		panel.setBounds(0, 0, 422, 565);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel label = new JLabel(" ");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setBounds(0, 13, 422, 246);
		label.setIcon(new ImageIcon(Home.class.getResource("/Img/logo-gsb.png")));
		panel.add(label);
		
		Button buttonlogin = new Button("Se connecter");
		buttonlogin.setFont(new Font("Dialog", Font.BOLD, 12));
		buttonlogin.setForeground(Color.WHITE);
		buttonlogin.setActionCommand(" ");
		buttonlogin.setBackground(new Color(100, 149, 237));
		buttonlogin.setBounds(448, 337, 299, 38);
		contentPane.add(buttonlogin);
		
		textName = new JTextField();
		textName.setFont(new Font("Tahoma", Font.BOLD, 14));
		textName.setBorder(null);
		textName.setBackground(Color.WHITE);
		textName.setOpaque(false) ;
		
		textName.setBounds(448, 157, 299, 29);
		contentPane.add(textName);
		textName.setColumns(10);
		
		TxtPass = new JPasswordField();
		TxtPass.setFont(new Font("Tahoma", Font.BOLD, 14));
		TxtPass.setBorder(null);
		TxtPass.setOpaque(false);
		TxtPass.setBackground(Color.WHITE);
		TxtPass.setBounds(448, 259, 299, 29);
		contentPane.add(TxtPass);
		
		JSeparator separator = new JSeparator();
		separator.setBackground(new Color(0, 0, 0));
		separator.setBounds(448, 184, 299, 2);
		contentPane.add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBackground(new Color(0, 0, 0));
		separator_1.setBounds(448, 286, 299, 2);
		contentPane.add(separator_1);
		
		JLabel lblNewLabel = new JLabel("Nom d'utilisateur :");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(448, 124, 136, 16);
		contentPane.add(lblNewLabel);
		
		JLabel lblMotDePasse = new JLabel("Mot de passe :");
		lblMotDePasse.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblMotDePasse.setBounds(448, 216, 106, 16);
		contentPane.add(lblMotDePasse);
		
		Label label__Exit = new Label("X");
		label__Exit.setFont(new Font("Bernard MT Condensed", Font.PLAIN, 24));
		label__Exit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				System.exit(0);
			}
		});
		label__Exit.setForeground(new Color(135, 206, 250));
		label__Exit.setBackground(new Color(230, 230, 250));
		label__Exit.setBounds(743, 10, 39, 29);
		contentPane.add(label__Exit);
		
		JLabel lblNewLabel_1 = new JLabel("Connexion");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 25));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setBounds(421, 66, 349, 29);
		contentPane.add(lblNewLabel_1);
		
		Label label_1 = new Label("-");
		label_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				setState(JFrame.ICONIFIED);
			}
		});
		label_1.setForeground(new Color(135, 206, 250));
		label_1.setFont(new Font("Bernard MT Condensed", Font.PLAIN, 34));
		label_1.setBackground(new Color(230, 230, 250));
		label_1.setBounds(715, 10, 22, 29);
		contentPane.add(label_1);
		
		
	}
}
